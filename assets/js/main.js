$(function () {
  $('.fecha-nerda').datepicker({dateFormat: 'yymmdd', dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'], monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'October', 'Noviembre', 'Diciembre']});
  $('.focus,#email').focus();
  $('form.no-enter input[type=text]').bind('keypress', function(e) {
    // 13: tecla Enter
    if (e.keyCode == 13) {
      var inp = e.target;
      var form = $(inp).closest('form');
      form[0].elements[inp.tabIndex+1].focus();
      return false;
    }
  });

  $('a.new-win').each(function () {
    this.target = '_blank';
  });
});
