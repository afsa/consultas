SELECT C.nOMABR as Comp,P.CORRE AS CorreOp,p.numero as Numero,p.fecemi as Fecha,p.articulo as Articulo, A.CODIGOSISANT as Codigo,  a.nombre as Descripcion,p.saldoca as Saldo
  FROM pendiesto p inner join articulos a on p.articulo = a.articulo  inner join cpbtes c on c.cpbte = p.cpbte
 WHERE p.cpbte in(825,826,861,862,863,864,865)
 ORDER BY p.cpbte,p.articulo,p.numero
