<?php

Atomik::set(array (
  'title' => 'AFSA',
  'company' => 'Ambassador Fueguina S.A.',
  'app' => 
  array (
    'layout' => '_layout',
    'default_action' => 'index',
    'views' => 
    array (
      'file_extension' => '.phtml',
      'contexts' => array (
        'xls' => array (
          'prefix' => false,
          'layout' => false,
          'content-type' => 'application/vnd.ms-excel',
        ),
      ),
    ),
    'helpers' => array (
    ),
  ),
  'atomik' => 
  array (
    'start_session' => true,
    'class_autoload' => true,
    'trigger' => 'action',
    'catch_errors' => true,
    'display_errors' => true,
    'debug' => false,
    'url_rewriting' => true,
  ),
  'plugins' => 
  array (
    'Db' => array (
      'dsn' => 'sqlsrv:server=serversql;database=Afsa',
      'username' => false,
      'password' => false,
    ),
  ),
  'styles' => 
  array (
    'assets/css/all.css',
    'assets/css/ui-lightness/jquery-ui-1.8.14.custom.css',
  ),
  'scripts' => 
  array (
    'assets/js/jquery-1.5.1.min.js',
    'assets/js/jquery-ui-1.8.14.custom.min.js',
    'assets/js/main.js',
  ),
  'menu' => array (
    'Inicio' => array (
      '\\' => 'Inicio',
      '\\articulos' => 'Artículos',
      '\\pproduccion' => 'Parte de producción',
      '\\dispositivos' => 'Imprimir x disp.',
      '\\imprSueltos' => 'Imprimir sueltos.',
      '\\serieFinal' => 'Serie final',
      '\\artProducido' => 'Artículo producido',
      '\\despachos' => 'Despachos',
      '\\ultimos' => 'Últimos producidos',
      '\\serieOP' => 'Serie y OP',
      '\\estado5' => 'Estado 5',
      '\\pendientesProd' => 'Órdenes pendientes',
      '\\limpiarInt' => 'Limpiar intercambio',
      '\\usuarios' => 'Usuarios',
    ),
  ),
  'auth' => array (
    'salt' => '«We decided to use Gitorious for our code because the service invites to collaboration.»',
    'userTypes' => array (
      'admin' => 10,
      'usuario' => 30,
      'visor' => 40,
    ),
  ),
));


Atomik::set('app/routes', array(
  'usuario/:id' => array(
    'action' => 'usuario',
  ),
  'serieFinal/:serie' => array(
    'action' => 'serieFinal',
  ),
  'estado5/:id' => array(
    'action' => 'estado5',
  ),
  'artProducido/:serie.:format' => array(
    'action' => 'artProducido',
  ),
  'artProducido/:serie' => array(
    'action' => 'artProducido',
  ),
  'despacho/:desp.:format' => array(
    'action' => 'despacho',
  ),
  'despacho/:desp' => array(
    'action' => 'despacho',
  ),
  'ultimos/:n.:format' => array(
    'action' => 'ultimos',
  ),
  'ultimos/:n' => array(
    'action' => 'ultimos',
  ),
  'serieOP/:serie/:dias' => array(
    'action' => 'serieOP',
    'serie' => 0,
    'dias' => 3,
  ),
  'imprimir/:id' => array(
    'action' => 'imprimir',
  ),
  'imprimirProd/:id' => array(
    'action' => 'imprimirProd',
  ),
  '#pproduccion-(?P<fecha1>[0-9]{8})-(?P<fecha2>[0-9]{8})\.(?P<format>.*)#' => array(
    'action' => 'pproduccion',
  ),
));
