<?php
class MyHashHelper {
  public function myHash($str) {
    return sha1(Atomik::get('auth/salt').$str);
  }
}
