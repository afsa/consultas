<?php
class ItemMenuHelper {
  public function itemMenu($subMenu) {
    if (isset($_SESSION['user'])) {
      $menu = A('menu/'.$subMenu);
      $i = 0;
      foreach ($menu as $url=>$item) {
        $url = str_replace('\\', '/', $url);
        $class = ++$i == count($menu)? ' class="last"': '';
?>  
          <li<?php echo $class;?>><a href="<?php echo Atomik::url($url);?>"><?php echo $item;?></a></li>
<?php
      }
    }
  }
}
