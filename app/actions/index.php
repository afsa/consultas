<?php
if (isset($_POST['email']) && isset($_POST['pass'])) {
  $email = $_POST['email'];
  $pass2 = $this->my_hash($_POST['pass']);

  // LOGIN
  // $rs = Atomik_Db::findAll('web_user', array('email'=>$email, 'pass'=>$pass2));
  // $user = $rs->fetchAll();
  $sql = 'SELECT * FROM web_user where (email = ? OR username = ?) AND pass = ?';
  $rs = Atomik_Db::query($sql, array($email, $email, $pass2));

  if ($rs !== FALSE) {
    $user = $rs->fetchAll();

    if (count($user)) {
      unset($user[0]['pass']);
      Atomik::set('session/user', $user[0]);
      Atomik::flash('Bienvenido '.$user[0]['name'], 'ok');
      Atomik::redirect('/usuarios');
    }
    else
      Atomik::flash('Error en email y/o contraseña', 'error');
  }
  else
    Atomik::flash('Error en email y/o contraseña', 'error');
}
else
  $email = '';
