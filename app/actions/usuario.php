<?php
if (!$this->logged())
  Atomik::redirect('/');

$id = isset($_REQUEST['id'])? $_REQUEST['id']: FALSE;
if (!is_null(Atomik::get('request/id')))
  $id = Atomik::get('request/id');
$name = isset($_POST['name'])? $_POST['name']: FALSE;
$email = isset($_POST['email'])? $_POST['email']: FALSE;
$username = isset($_POST['username'])? $_POST['username']: FALSE;
$pass = isset($_POST['pass'])? $_POST['pass']: FALSE;
$repass = isset($_POST['repass'])? $_POST['repass']: FALSE;
$type = isset($_POST['type'])? $_POST['type']: A('auth/userTypes/usuario');
$active = isset($_POST['active'])? TRUE: count($_POST) == 0;

$isUpd = $id !== FALSE;

if (A('session/user/type') != A('auth/userTypes/admin') && (!$isUpd || ($isUpd && $id != A('session/user/id'))))
  Atomik::redirect('/usuario/'.A('session/user/id'));

if (count($_POST) && $email && $name) {
  if ($pass == $repass) {
    $updated = date('Ymd H:i:s');
    $active2 = $active? 1: 0;
    $pass2 = $this->my_hash($pass);
    if ($isUpd) {
      $params = array('username' => $username, 'name' => $name, 'email' => $email, 'type' => $type, 'active' => $active2, 'updated' => $updated);
      if ($pass != '')
        $params['pass'] = $pass2;
      $sql = Atomik_Db::update('web_user', $params, array('id' => $id));
      unset($params);
      if ($sql !== FALSE) {
        Atomik::flash('Usuario actualizado correctamente', 'ok');
        Atomik::redirect('/usuarios');
      }
    }
    else {
      $created = $updated;
      $sql = Atomik_Db::insert('web_user', array('username' => $username, 'name' => $name, 'email' => $email, 'pass' => $pass2, 'type' => $type, 'active' => $active2, 'created' => $created, 'updated' => $updated));
      if ($sql !== FALSE) {
        Atomik::flash('Usuario creado correctamente', 'ok');
        Atomik::redirect('/usuarios');
      }
      else
        Atomik::flash('No se creó el usuario, vuelva a intentarlo o consulte con el administrador del sistema', 'error');
    }
  }
  else
    Atomik::flash('No coincide la contraseña y su confirmación', 'error');
}
else
{
  if (count($_POST))
    Atomik::flash('Verifique los campos marcados con *', 'error');
  else {
    if ($isUpd) {
      $usuarios = Atomik_Db::query('SELECT * FROM web_user WHERE id = ?', array($id));
      if ($usuarios)
        $usuario = $usuarios->fetchAll();
      if (count($usuario)) {
        $name     = $usuario[0]['name'];
        $email    = $usuario[0]['email'];
        $username = $usuario[0]['username'];
        $type     = $usuario[0]['type'];
        $active   = $usuario[0]['active'];
      }
      else {
        Atomik::flash('Usuario inexistente', 'error');
        Atomik::redirect('/usuarios');
      }
    }
  }
}
$pass = '';
// echo Atomik::get('session/username');

$types = A('auth/userTypes');
