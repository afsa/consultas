<?php
if (!$this->logged())
  Atomik::redirect('/');

if (isset($_GET['serie']))
  Atomik::redirect('/serieFinal/'.$_GET['serie']);
$serie = !is_null(Atomik::get('request/serie'))? Atomik::get('request/serie'): '';

if ($serie != '') {
  $sql = "
select orden = case n.nroserie
         when i.dato02 then 1
         when i.dato03 then 2
         when i.dato04 then 3
         when i.dato05 then 4
         when i.dato06 then 5
       end,
       i.idintercambio, i.estado, a.articulo, a.nomabr, n.nroserie, i.dato01, i.dato02, i.dato03, i.dato04, i.dato05, i.dato06, a.nombre nombreArt
  from nrosserie n, articulos a, ZCRWNIntercambio i
 where n.articulo = a.articulo
   and i.dato06 = '%nroserie%'
   and (n.nroserie = i.dato02 OR n.nroserie = i.dato03 OR n.nroserie = i.dato04 OR n.nroserie = i.dato05 OR n.nroserie = i.dato06)
 order by i.idintercambio desc, orden
";
  $sql = str_replace(array("\r", "\n", '%nroserie%'), array('', ' ', $serie), $sql);
  $series = A('db:'.$sql)->fetchAll();
}
