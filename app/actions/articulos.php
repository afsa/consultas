<?php
if (!$this->logged())
  Atomik::redirect('/');

$params = Atomik::get('request');
$isNotXls = $params['format'] != 'xls';

$articulos = A('db:articulos')->fetchAll();
