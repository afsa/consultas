<?php
if (!$this->logged())
  Atomik::redirect('/');

if (isset($_GET['serie']))
  Atomik::redirect('/artProducido/'.$_GET['serie']);

$params = Atomik::get('request');
$isNotXls = (!isset($params['format']) || $params['format'] != 'xls');

$serie = isset($params['serie'])? $params['serie']: '';

if ($serie != '') {
  $sql = '
select
 AI.articulo, Ac.Nombre as NombreArticulo, Ai.CodigoSisAnt as Insumo, Ai.Nombre as NombreInsumo, DE.FechaAlta, HI.Despacho, CI.NroSerieInsumo NroSerie
from
 CierreProdCab CC
inner join
 Articulos AC on AC.Articulo=CC.Articulo
inner join
 CierreProdDet CI on ci.CorreCierre=cc.CorreCierre
inner join
 Articulos Ai on Ai.Articulo=CI.ArticuloInsumo
inner join
 HistoSto HI on HI.articulo = AI.articulo
left join
 Despachos DE on DE.despacho = HI.despacho
where
 CC.nroserie = %nroserie%
group by
 AI.articulo, Ac.CodigoSisAnt,ac.nombre,  Ai.Nombre, Ai.CodigoSisAnt, DE.fechaalta, HI.despacho, CI.NroSerieInsumo
order by
 NombreArticulo, Insumo, DE.FechaAlta desc
';
  $sql = str_replace(array("\r", "\n", '%nroserie%'), array('', ' ', $serie), $sql);
  $articulos = A('db:'.$sql)->fetchAll();
  $rs = array();
  $lastArt = 0;
  foreach ($articulos as $reg) {
    if ($reg['articulo'] != $lastArt)
      $rs[] = $reg;
    $lastArt = $reg['articulo'];
  } 
  unset($articulos, $reg, $lastArt);
}
