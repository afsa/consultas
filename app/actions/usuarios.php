<?php
if ($this->logged()) {
  if (A('session/user/type') == A('auth/userTypes/admin')) {
    $usuarios = A('db:web_user')->fetchAll();
  }
  else {
    $rs = Atomik_Db::findAll('web_user', array('id'=>A('session/user/id')));
    $usuarios = $rs->fetchAll();
    unset($rs);
  }
}
else
  Atomik::redirect('/');
