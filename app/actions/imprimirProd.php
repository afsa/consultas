<?php
if (!$this->logged())
  Atomik::redirect('/');

$params = Atomik::get('request');
if (!isset($params['id']))
  Atomik::redirect('/dispositivos');

include('_printerFuncs.php');

$id = $params['id'];
if (isset($_POST['dispositivo']))
  $id = $_POST['dispositivo'];
$impresora = getImpresora($id);
if (!$impresora)
  Atomik::redirect('/dispositivos');

$dispositivos = A('db:SELECT NroDispositivo id, Descripcion descr FROM zcrwndispositivos WHERE Tipo = 2')->fetchAll();

$sector = $producto = $orden = '';
$cantidad = 1;
if (isset($_POST['orden'])) {
  $orden = $_POST['orden'];
  if (isset($_POST['cantidad'])) $cantidad = $_POST['cantidad'];
  if (isset($_POST['sector'  ])) $sector   = $_POST['sector'];
  if (isset($_POST['producto'])) $producto = $_POST['producto'];
  $printStr = getPrintStr($cantidad, $sector, $producto, $orden);
  $ret = imprimir($impresora, $printStr);
}

function getPrintStr($cantidad, $sector, $producto, $orden) {
  // VER formato0100.lbl
  $ret = '
! 0 100 400 1
PITCH 200
WIDTH 230
U A30 (3,0,0) 1 5 Orden Produccion:
U A30 (3,0,0) 1 55 Sector:
U A30 (3,0,0) 1 105 Codigo Producto:
U A30 (3,0,0) 1 155 Cantidad:
U A30 (2,0,0) 235 8 %ORDEN%
U A30 (2,0,0) 110 58 %SECTOR%
U A30 (2,0,0) 220 108 %PRODUCTO%
U A30 (2,0,0) 135 158 %CANTIDAD%
BARCODE CODE128-(2:3) 70 340 135 %ORDEN%
U A25 (2,0,0) 120 350 %ORDEN%
END
';
  $ret = str_replace(array('%CANTIDAD%', '%SECTOR%', '%PRODUCTO%', '%ORDEN%'), array($cantidad, $sector, $producto, $orden), $ret);
  return $ret;
}
