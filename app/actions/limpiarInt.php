<?php
if (!$this->logged() || A('session/user/type') != A('auth/userTypes/admin'))
  $this->redirect('/');

$fecha = '';
if (count($_POST)) { 
  $fecha = $_POST['fecha'];
  if (isset($_POST['confirma']) && isset($_POST['conteo']) && $_POST['confirma'] == 1) {
    $conteo = $_POST['conteo'];
    $sql = "DELETE FROM ZCRWNIntercambio WHERE estado = 1 AND (accion = 2 OR (nrodispositivo = 6 AND accion in (1,22))) AND fecemi < '%fecha%' ";
    $sql = str_replace('%fecha%', $fecha, $sql);
    Atomik_Db::query($sql);
    Atomik::flash('Se borrarron '.$conteo.' registros de la tabla de intercambio', 'ok');
  }
  else {
    $sql = "
SELECT COUNT(*) conteo
  FROM ZCRWNIntercambio
 WHERE estado = 1
   AND (accion = 2 OR (nrodispositivo = 6 AND accion in (1,22)))
   AND fecemi < '%fecha%'
";
    $sql = str_replace(array("\r", "\n", '%fecha%'), array('', ' ', $fecha), $sql);
    $conteo = A('db:'.$sql)->fetchAll();
    unset($sql);
  }
}
