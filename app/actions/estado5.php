<?php
if (!$this->logged())
  Atomik::redirect('/');

if (isset($_GET['id']))
  Atomik::redirect('/estado5/'.$_GET['id']);
$id = !is_null(Atomik::get('request/id'))? Atomik::get('request/id'): '';

if ($id != '') {
  $sql = "
select 
IDINTERCAMBIO,articulo,codart,descripcion,nroserie,deposito,nivelsto,cpbte,nrocpbte,estado
 from
(
select Z.IDINTERCAMBIO AS IDINTERCAMBIO,n.articulo as Articulo, a.codigosisant as CodArt,a.nombre as Descripcion,n.nroserie as NroSerie,n.deposito as Deposito,n.nivelsto as NivelSto,	0 as cpbte,0 as nrocpbte,'Disponible' as Estado 
from nrosserie n
inner join articulos a on a.articulo = n.articulo
inner join zcrwnintercambio z on n.nroserie  = z.dato02  or n.nroserie  = z.dato03 or n.nroserie  = z.dato04  or n.nroserie  = z.dato05
where  z.estado = 5 and n.nroserie in(z.dato02,z.dato03,z.dato04,z.dato05) and z.idintercambio = %idintercambio%

union all
select Z.IDINTERCAMBIO AS IDINTERCAMBIO,c.articuloinsumo as Articulo,a.codigosisant as CodArt,a.nombre as Descripcion,c.nroserieinsumo as Nroserie,0 as deposito,0 as nivelsto,c.cpbteInsumo as cpbte,c.numeroinsumo as NroCpbte,'Consumido' as Estado
from cierreproddet c inner join articulos a on c.articuloinsumo = a.articulo
inner join zcrwnintercambio z on c.nroserieinsumo = z.dato02 or c.nroserieinsumo = z.dato03 or c.nroserieinsumo  = z.dato04  or c.nroserieinsumo  = z.dato05
where   z.estado = 5 and c.nroserieinsumo  in(z.dato02,z.dato03,z.dato04,z.dato05)  and z.idintercambio = %idintercambio%
) as S

order by nroserie
";
  $sql = str_replace(array("\r", "\n", '%idintercambio%'), array('', ' ', $id), $sql);
  $rs = A('db:'.$sql)->fetchAll();
}
