<?php
if (!$this->logged())
  Atomik::redirect('/');

$params = Atomik::get('request');
$isNotXls = !isset($params['format']) || $params['format'] != 'xls';

$fecha1 = isset($params['fecha1'])? $params['fecha1']: (isset($_GET['fecha1'])? $_GET['fecha1']: date('Ymd'));
$fecha2 = isset($params['fecha2'])? $params['fecha2']: (isset($_GET['fecha2'])? $_GET['fecha2']: date('Ymd'));

$sql = '
SELECT COUNT(*) cant
  FROM ZCRWNIntercambio
 WHERE estado > 1
   AND Dato01 LIKE \'*PT%\'
   AND FecEmi >= \'%fecha1%\' and FecEmi < DateAdd(d, 1, \'%fecha2%\')
';
$sql = str_replace(array("\r", "\n", '%fecha1%', '%fecha2%'), array('', ' ', $fecha1, $fecha2), $sql);
$erres = A('db:'.$sql)->fetchAll();

$sql = '
SELECT A.CODIGOSISANT as codigo, A.NOMBRE as nombre, SUM(EMBALADOS) AS embalados,  SUM(ARMADOS) AS armados, sum(TRANSFERIDO) AS transferidos
  from (
        SELECT A.ARTICULO, COUNT(DISTINCT ZI.NROSERIE ) AS EMBALADOS  , 0 AS ARMADOS  , 0 as TRANSFERIDO
          FROM ZCRWNProduccionDiaria ZI
          LEFT JOIN NrosSerie NSA ON NSA.NroSerie=ZI.NroSerie
          LEFT JOIN BajaNrosSerie NSB ON NSB.NroSerie=ZI.NroSerie
          left JOIN Articulos A ON A.Articulo=isNull(isNull(NSA.Articulo,NSB.Articulo),0)
         WHERE ZI.FecEmi >= \'%fecha1%\' and ZI.FecEmi < DateAdd(d, 1, \'%fecha2%\')
           and zi.nroserie like \'9%\'
         GROUP BY A.Articulo
         UNION ALL
        SELECT C.Articulo, 0 AS  EMBALADOS, COUNT(C.NROSERIE) AS ARMADOS, 0 as TRANSFERIDO
          FROM CIERREPRODCAB C
         WHERE CPBTECIERRE IN (835,836,871,872,873,874,875,883,884,885,845,846)
           AND c.FecEmiCierre >= \'%fecha1%\' and C.FecEmiCierre < DateAdd(d, 1, \'%fecha2%\')
         GROUP BY c.articulo
         UNION ALL
        SELECT  C.Articulo, 0 AS  EMBALADOS, 0 AS ARMADOS, COUNT(C.NROSERIE) as TRANSFERIDO
          FROM HISTOSTO C
         WHERE NIVELSTO=999930
           AND CPBTE IN (910)
           AND DEPOSITO=114
           AND DEBECA=0
           AND C.Empresa=10
           and c.FecEmi >= \'%fecha1%\' and c.FecEmi < DateAdd(d, 1, \'%fecha2%\')
         GROUP BY c.articulo
        ) AS  P  INNER JOIN ARTICULOS A ON A.ARTICULO = P.ARTICULO
 group by a.codigosisant, a.NOMBRE
 order by A.CODIGOSISANT, A.NOMBRE
';

$sql = str_replace(array("\r", "\n", '%fecha1%', '%fecha2%'), array('', ' ', $fecha1, $fecha2), $sql);
$articulos = A('db:'.$sql)->fetchAll();

unset($params, $sql);
