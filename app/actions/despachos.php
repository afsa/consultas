<?php
if (!$this->logged())
  Atomik::redirect('/');

$params = Atomik::get('request');
$isNotXls = (!isset($params['format']) || $params['format'] != 'xls');

$sql = 'SELECT Despacho, FechaAlta, FechaMod FROM despachos ORDER BY FechaAlta DESC';
$despachos = A('db:'.$sql)->fetchAll();
