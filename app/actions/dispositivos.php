<?php
if (!$this->logged())
  Atomik::redirect('/');

$params = Atomik::get('request');
$isNotXls = $params['format'] != 'xls';
unset($params);

$dispositivos = A('db:zcrwndispositivos')->fetchAll();
