<?php
if (!$this->logged())
  Atomik::redirect('/');

$params = Atomik::get('request');
if (!isset($params['id']) || !is_numeric($params['id']))
  Atomik::redirect('/dispositivos');

include('_printerFuncs.php');

$id = $params['id'];
if (isset($_POST['dispositivo']))
  $id = $_POST['dispositivo'];
$impresora = getImpresora($id);
if (!$impresora)
  Atomik::redirect('/dispositivos');

$formatos = array(
  1 => 'Chica',
  2 => 'Grande',
  3 => 'Grande RRR',
);

$dispositivos = A('db:SELECT NroDispositivo id, Descripcion descr FROM zcrwndispositivos WHERE Tipo = 2')->fetchAll();

$formato = isset($_POST['formato'])? $_POST['formato']: $formatos[1];
$desde = $hasta = 1;
$titulo = '04GF2908-B';

if (isset($_POST['desde'])) {
  $desde  = $_POST['desde'];
  $hasta  = isset($_POST['hasta' ])? $_POST['hasta' ]: $desde;
  $titulo = isset($_POST['titulo'])? $_POST['titulo']: $titulo;
  $printStr = getPrintStr($formato, $desde, $hasta, $titulo);
  $ret = imprimir($impresora, $printStr);
}

function getPrintStr($formato, $desde, $hasta, $titulo) {
  // VER formato0001.lbl
  // VER formato0002.lbl
  // VER formato0003.lbl
  switch($formato) {
    case 1:
      $cantidad = ceil(($hasta - $desde + 1) / 2);
      $ret = '
! 0 100 150 %CANTIDAD%
PITCH 200
WIDTH 240
JUSTIFY CENTER 
U A23 (3,0,0) 105 8 %PRODUCTO%
BARCODE CODE128-(2:3) 10 112 72 %BARRA%
ADJUST 02
U A22 (2,0,0) 105 121 %NUMERO%
ADJUST 02

U A23 (3,0,0) 360 8 %PRODUCTO%
BARCODE CODE128-(2:3) 265 112 72 %BARRA1%
ADJUST 02
U A22 (2,0,0) 360 121 %NUMERO1%
ADJUST 02
END
';
      $ret = str_replace(array('%CANTIDAD%', '%PRODUCTO%', '%BARRA%', '%NUMERO%', '%BARRA1%', '%NUMERO1%'), array($cantidad, $titulo, $desde, $desde, $desde+1, $desde+1), $ret);
    break;
    case 2:
      $cantidad = $hasta - $desde + 1;
      $ret = '
! 0 100 400 %CANTIDAD%
PITCH 200
WIDTH 230
JUSTIFY CENTER
U A25 (3,0,0) 215 65 %TITULO%
BARCODE CODE128-(3:5) 70 310 200 %BARRA%
ADJUST 01
U A27 (2,0,0) 215 330 %NUMERO%
ADJUST 01
END
';
      $ret = str_replace(array('%CANTIDAD%', '%TITULO%', '%BARRA%', '%NUMERO%'), array($cantidad, $titulo, $desde, $desde), $ret);
    break;
    case 3:
      $cantidad = $hasta - $desde + 1;
      $ret = '
! 0 100 400 %CANTIDAD%
PITCH 200
WIDTH 230
JUSTIFY CENTER
U A25 (3,0,0) 215 45 %TITULO%
BARCODE CODE128-(3:5) 70 270 200 %BARRA%
ADJUST 01
U A27 (2,0,0) 215 290 %NUMERO%
ADJUST 01
U A27 (2,0,0) 215 325 RRRRRRRRRRRRRRRRRRR
END
';
      $ret = str_replace(array('%CANTIDAD%', '%TITULO%', '%BARRA%', '%NUMERO%'), array($cantidad, $titulo, $desde, $desde), $ret);
    break;
  }
  return $ret;
}
