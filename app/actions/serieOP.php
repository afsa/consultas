<?php
if (!$this->logged())
  Atomik::redirect('/');

$params = Atomik::get('request');

$serie = isset($_GET['serie'])? $_GET['serie']: (isset($params['serie'])? $params['serie']: 0);
$dias  = isset($_GET['dias' ])? $_GET['dias' ]: (isset($params['dias' ])? $params['dias' ]: 3);
if (isset($_GET['serie']) || isset($_GET['dias']))
  Atomik::redirect('/'.$params['action'].'/'.$serie.'/'.$dias);

if ($serie == 0)
  $serie = '';

$isNotXls = (!isset($params['format']) || $params['format'] != 'xls');

if ($serie != '') {
  $sql = '
SELECT a.articulo, a.nomabr, a.codigosisant codigo, a.nombre, s.fechaalta, s.fechamod
  FROM nrosserie s
 INNER join articulos a
    ON a.articulo = s.articulo
 WHERE s.nroserie = %nroserie%
';
  $sql = str_replace(array("\r", "\n", '%nroserie%'), array('', ' ', $serie), $sql);
  $series = A('db:'.$sql)->fetchAll();
  if (isset($series[0])) {
    $sql = "
SELECT DISTINCT h.cpbte, c.nomabr, c.nombre, a.codigosisant, h.fecemi fechaemi, h.numero
  FROM histocab h
 INNER JOIN histosto i
    ON i.corre = h.corre
 INNER JOIN articulos a
    ON a.articulo = i.articulo
 INNER JOIN agrupacpb c
    ON c.cpbte = h.cpbte
 WHERE DATEADD(day, -%days%, h.fecemi) <= '%fecha%'
   AND DATEADD(day,  %days%, h.fecemi) >= '%fecha%'
   AND h.cpbte < 870
 ORDER BY h.fecemi, h.cpbte
";
    $fecha = DateTime::createFromFormat('Y-m-d H:i:s.u', $series[0]['fechaalta']);
    $sFecha = $fecha->format('Ymd');
    $sql = str_replace(array("\r", "\n", '%fecha%', '%days%'), array('', ' ', $sFecha, $dias), $sql);
    unset($sFecha);
    $comprobantes = A('db:'.$sql)->fetchAll();
  }
}
