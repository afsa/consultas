<?php
if (!$this->logged())
  Atomik::redirect('/');

if (isset($_GET['desp']))
  Atomik::redirect('/despacho/'.$_GET['desp']);

$params = Atomik::get('request');
$isNotXls = (!isset($params['format']) || $params['format'] != 'xls');

$desp = isset($params['desp'])? $params['desp']: '';
if ($desp == '')
  Atomik::redirect('/despachos');

$sql = "
select h.articulo CodArticulo, a.NomAbr, a.CodigoSisAnt, .a.nombre Articulo, sum(h.debeca) cuenta
  from histosto h, articulos a
 where h.despacho = '%despacho%'
   and h.debeca > 0
   and a.articulo = h.articulo
 group by h.articulo, a.NomAbr, a.CodigoSisAnt, a.nombre
 order by h.articulo
";
$sql = str_replace(array("\r", "\n", '%despacho%'), array('', ' ', $desp), $sql);
$articulos = A('db:'.$sql)->fetchAll();
$sql = "SELECT Despacho, FechaAlta, FechaMod FROM despachos WHERE despacho = '%despacho%' ORDER BY FechaAlta DESC";
$sql = str_replace('%despacho%', $desp, $sql);
$despacho = A('db:'.$sql)->fetchAll();
