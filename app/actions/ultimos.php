<?php
// Ultimos productos terminados en estado OK

if (!$this->logged())
  Atomik::redirect('/');

if (isset($_GET['n']))
  Atomik::redirect('/ultimos/'.$_GET['n']);

$params = Atomik::get('request');
$isNotXls = (!isset($params['format']) || $params['format'] != 'xls');
$n = isset($params['n'])? $params['n']: 20;

$sql = "
SELECT TOP %n% idintercambio, fecemi, dato01 orden, dato02, dato03, dato04, dato05, dato06 serie
  FROM zcrwnintercambio
 WHERE estado = 1
   AND ((accion = 2 AND nrodispositivo = 4) OR (nrodispositivo = 6 AND accion = 22))
 ORDER by idintercambio desc
";
$sql = str_replace(array("\r", "\n", '%n%'), array('', ' ', $n), $sql);
$ultimos = A('db:'.$sql)->fetchAll();
