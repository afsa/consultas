<?php
if (!$this->logged())
  Atomik::redirect('/');

include('_printerFuncs.php');

$tpl=array('
! 0 100 150 1
PITCH 200
WIDTH 240
JUSTIFY CENTER 

U A23 (3,0,0) 105 8 %TITLE%
BARCODE CODE128-(2:3) 10 112 72 %CODE%
U A22 (2,0,0) 105 121 %CODE%
',
'
U A23 (3,0,0) 360 8 %TITLE%
BARCODE CODE128-(2:3) 265 112 72 %CODE%
U A22 (2,0,0) 360 121 %CODE%
','
END
');

$tpl2 ='
! 0 100 400 1
PITCH 200
WIDTH 230
JUSTIFY CENTER
U A25 (3,0,0) 215 65 %TITLE%
BARCODE CODE128-(3:5) 70 310 200 %CODE%
ADJUST 01
U A27 (2,0,0) 215 330 %CODE%
ADJUST 01
END
';

$tpl3 ='
! 0 100 400 1
PITCH 200
WIDTH 230
JUSTIFY CENTER
U A25 (3,0,0) 215 45 %TITLE%
BARCODE CODE128-(3:5) 70 270 200 %CODE%
ADJUST 01
U A27 (2,0,0) 215 290 %CODE%
ADJUST 01
U A27 (2,0,0) 215 325 RRRRRRRRRRRRRRRRRRR
END
';

$formatos = array(
  1 => 'Chica',
  2 => 'Grande',
  3 => 'Grande RRR',
);
$formato = isset($_POST['formato'])? $_POST['formato']: $formatos[1];

$dispositivos = A('db:SELECT NroDispositivo id, Descripcion descr FROM zcrwndispositivos WHERE Tipo = 2')->fetchAll();

$impr = $impresora = 0;
$etiquetas = $titulo = '';

if (isset($_POST['impresora']))
  $impr = $_POST['impresora'];
if (isset($_POST['titulo']))
  $titulo = $_POST['titulo'];
if (isset($_POST['etiquetas'])) {
  $etiquetas = $_POST['etiquetas'];

  switch($formato) {
    case 2:
      $str = genPrinterStr2($tpl2, $_POST['etiquetas'], $_POST['titulo']);
      break;
    case 3:
      $str = genPrinterStr2($tpl3, $_POST['etiquetas'], $_POST['titulo']);
      break;
    default:
      $str = genPrinterStr($tpl, $_POST['etiquetas'], $_POST['titulo']);
  }

  $impresora = getImpresora($impr);
  $ret = imprimir($impresora, $str);
}

function genPrinterStr($tpl, $etiquetas, $titulo) {
  $str = '';
  $i = 0;
  $etiq = _preprocEtiq($etiquetas);

  for (; isset($etiq[$i]); $i++) {
    $str .= str_replace('%CODE%', $etiq[$i], $tpl[$i%2]);
    if ($i%2) $str .= $tpl[2];
  }
  if ($i%2) $str .= $tpl[2];

  $str = str_replace('%TITLE%', $titulo, $str);
  return $str;
}

function genPrinterStr2($tpl, $etiquetas, $titulo) {
  $str = '';
  $etiq = _preprocEtiq($etiquetas);

  for ($i = 0; isset($etiq[$i]); $i++) {
    if ($etiq[$i] != '')
      $str .= str_replace('%CODE%', $etiq[$i], $tpl);
  }
  $str = str_replace('%TITLE%', $titulo, $str);
  return $str;
}

function _preprocEtiq($etiquetas) {
  $etiquetas = str_replace("\r\n", "\n", $etiquetas);
  $etiquetas = str_replace("\r", "\n", $etiquetas);
  $ret = explode("\n", $etiquetas);
  for($i = 0; isset($ret[$i]); $i++)
    $ret[$i] = trim($ret[$i]);
  return $ret;
}
