<?php
function getImpresora($id) {
  $sql = '
SELECT *
  FROM zcrwndispositivos
 WHERE nrodispositivo = %id%
';
  $sql = str_replace(array("\r", "\n", '%id%'), array('', ' ', $id), $sql);
  $ret = A('db:'.$sql)->fetchAll();
  return isset($ret[0])? $ret[0]: FALSE;
}

function imprimir($impresora, $str) {
  $socket  = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
  if ($socket === FALSE)
    return FALSE;
  $ok = socket_connect($socket, $impresora['IPDispositivo'], $impresora['Puerto']);
  if ($ok === FALSE)
    return FALSE;
  $ok = socket_write($socket, $str);
  if ($ok === FALSE)
    return FALSE;
  socket_close($socket);
  return true;
}
